import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <List/>
    );
  }
}

class Task extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      isUndone: true,
      taskString: props.taskString
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState(prevState => ({
      isUndone: !prevState.isUndone
    }));
  }

  render() {
    let header = this.state.isUndone ? 'aliveButton' : 'deadButton';
    return (
      <li>
        <button className={header} onClick={this.handleClick}>
          {this.state.taskString}
        </button>
      </li>
    );
  }

}

class List extends React.Component{

  constructor(props){
    super(props);
    this.state = {
      value : '',
      taskList : [],
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleInsert = this.handleInsert.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  handleChange(event) {
      this.setState({value: event.target.value.toUpperCase()});
    }

  handleInsert(event) {
    this.setState( (prevState) => ({
      taskList : [...prevState.taskList, prevState.value],
    }));
    event.preventDefault();
  }

  handleDelete(taskString, e){
    this.setState(prevState => ({
      taskList: prevState.taskList.filter((o) => {
        if (o != taskString) return o
      })
    }));
    e.preventDefault();
  }

  render(){
    const printList = this.state.taskList.map( (o) => (
      <div id="inner">
        <Task taskString={o} key={o} />
        <button onClick={(e) => this.handleDelete(o, e)} className='deleteButton'> DELETE </button>
      </div>
    ));

    return (
      <div>
        <form onSubmit={this.handleInsert}>
          <label>
            Add Task:
            <input type="text" name="task" value={this.state.value} onChange={this.handleChange}/>
          </label>
          <input type="submit" value="Enter Task"/>
        </form>
        <ul> {printList} </ul>
      </div>
    );
  }
}

export default App;
